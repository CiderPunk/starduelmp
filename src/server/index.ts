#!/usr/bin/env node
"use strict";

import * as Http from "http";
import * as Express from "express";
import * as Io from "socket.io";

const app = Express();


app.use('/src', Express.static('src'));
app.use('/dist', Express.static('dist'));
app.use('/node_modules', Express.static('node_modules'));
app.use(Express.static('public'));

const server = app.listen(8000,function(){ 
  console.log('listening on *:8000');
});



//create socket listener
const io = Io(server);
io.on('connection',function(socket){
  console.log('a user connected: ' + socket.id);
  socket.on('disconnect', function(){
    console.log('user disconnected: ' + socket.id);
  });
});
